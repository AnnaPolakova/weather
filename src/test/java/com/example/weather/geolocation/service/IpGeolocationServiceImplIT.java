package com.example.weather.geolocation.service;

import com.example.weather.geolocation.dto.IpGeolocationDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IpGeolocationServiceImplIT {

    @Autowired
    private IpGeolocationService ipGeolocationService;

    private static final String RIGA_IP = "82.193.71.23";
    private static final String RIGA_LATITUDE = "56.9496";
    private static final String RIGA_LONGITUDE = "24.0978";

    @Test
    public void saveAndGetIpGeolocation() {
        ipGeolocationService.saveIpGeolocation(RIGA_IP, RIGA_LATITUDE, RIGA_LONGITUDE);
        IpGeolocationDTO rigaIpGeolocation = ipGeolocationService.getIpGeolocationByIpAddress(RIGA_IP);
        assertEquals(rigaIpGeolocation.getIpAddress(), RIGA_IP);
        assertEquals(rigaIpGeolocation.getLatitude(), RIGA_LATITUDE);
        assertEquals(rigaIpGeolocation.getLongitude(), RIGA_LONGITUDE);
    }
}