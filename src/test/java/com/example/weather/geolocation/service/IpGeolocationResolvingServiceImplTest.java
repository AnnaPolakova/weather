package com.example.weather.geolocation.service;

import com.example.weather.geolocation.dto.IpGeolocationDTO;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class IpGeolocationResolvingServiceImplTest {

    private static final String RIGA_IP = "82.193.71.23";
    private static final String RIGA_LATITUDE = "56.9496";
    private static final String RIGA_LONGITUDE = "24.0978";

    private static final String PARIS_IP = "146.112.128.150";
    private static final String PARIS_LATITUDE = "48.8607";
    private static final String PARIS_LONGITUDE = "2.3281";

    @InjectMocks
    private IpGeolocationResolvingServiceImpl ipGeolocationResolvingServiceImpl;

    @Mock
    private LocalIpAddressProvider localIpAddressProvider;
    @Mock
    private IpGeolocationService ipGeolocationService;

    @Test
    public void getLocalhostGeolocation() throws IOException, GeoIp2Exception {
        doReturn(RIGA_IP).when(localIpAddressProvider).getLocalIpAddress();
        doReturn(null).when(ipGeolocationService).getIpGeolocationByIpAddress(RIGA_IP);
        IpGeolocationDTO expectedIpGeolocationRiga = new IpGeolocationDTO(RIGA_IP, RIGA_LATITUDE, RIGA_LONGITUDE);
        doReturn(expectedIpGeolocationRiga).when(ipGeolocationService).saveIpGeolocation(
                expectedIpGeolocationRiga.getIpAddress(),
                expectedIpGeolocationRiga.getLatitude(),
                expectedIpGeolocationRiga.getLongitude());

        IpGeolocationDTO rigaIpGeolocation = ipGeolocationResolvingServiceImpl.getLocalhostGeolocation();
        assertEquals(rigaIpGeolocation.getIpAddress(), expectedIpGeolocationRiga.getIpAddress());
        assertEquals(rigaIpGeolocation.getLatitude(), expectedIpGeolocationRiga.getLatitude());
        assertEquals(rigaIpGeolocation.getLongitude(), expectedIpGeolocationRiga.getLongitude());

        doReturn(PARIS_IP).when(localIpAddressProvider).getLocalIpAddress();
        doReturn(null).when(ipGeolocationService).getIpGeolocationByIpAddress(PARIS_IP);
        IpGeolocationDTO expectedIpGeolocationParis = new IpGeolocationDTO(PARIS_IP, PARIS_LATITUDE, PARIS_LONGITUDE);
        doReturn(expectedIpGeolocationParis).when(ipGeolocationService).saveIpGeolocation(
                expectedIpGeolocationParis.getIpAddress(),
                expectedIpGeolocationParis.getLatitude(),
                expectedIpGeolocationParis.getLongitude());

        IpGeolocationDTO parisIpGeolocation = ipGeolocationResolvingServiceImpl.getLocalhostGeolocation();
        assertEquals(parisIpGeolocation.getIpAddress(), expectedIpGeolocationParis.getIpAddress());
        assertEquals(parisIpGeolocation.getLatitude(), expectedIpGeolocationParis.getLatitude());
        assertEquals(parisIpGeolocation.getLongitude(), expectedIpGeolocationParis.getLongitude());
    }
}