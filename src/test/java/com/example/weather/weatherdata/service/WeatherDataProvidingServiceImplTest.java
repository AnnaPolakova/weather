package com.example.weather.weatherdata.service;

import com.example.weather.geolocation.dto.IpGeolocationDTO;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.example.weather.geolocation.service.IpGeolocationResolvingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WeatherDataProvidingServiceImplTest {

    private static final String WEATHER_DATA = "{\n" +
            "    \"coord\": {\n" +
            "        \"lon\": 24.0978,\n" +
            "        \"lat\": 56.9496\n" +
            "    },\n" +
            "    \"weather\": [\n" +
            "        {\n" +
            "            \"id\": 211,\n" +
            "            \"main\": \"Thunderstorm\",\n" +
            "            \"description\": \"thunderstorm\",\n" +
            "            \"icon\": \"11d\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 500,\n" +
            "            \"main\": \"Rain\",\n" +
            "            \"description\": \"light rain\",\n" +
            "            \"icon\": \"10d\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"base\": \"stations\",\n" +
            "    \"main\": {\n" +
            "        \"temp\": 295.2,\n" +
            "        \"feels_like\": 295.63,\n" +
            "        \"temp_min\": 295.2,\n" +
            "        \"temp_max\": 298.66,\n" +
            "        \"pressure\": 1013,\n" +
            "        \"humidity\": 83\n" +
            "    },\n" +
            "    \"visibility\": 10000,\n" +
            "    \"wind\": {\n" +
            "        \"speed\": 2.57,\n" +
            "        \"deg\": 150\n" +
            "    },\n" +
            "    \"rain\": {\n" +
            "        \"1h\": 0.15\n" +
            "    },\n" +
            "    \"clouds\": {\n" +
            "        \"all\": 75\n" +
            "    },\n" +
            "    \"dt\": 1626277402,\n" +
            "    \"sys\": {\n" +
            "        \"type\": 1,\n" +
            "        \"id\": 1876,\n" +
            "        \"country\": \"LV\",\n" +
            "        \"sunrise\": 1626227425,\n" +
            "        \"sunset\": 1626289686\n" +
            "    },\n" +
            "    \"timezone\": 10800,\n" +
            "    \"id\": 6615326,\n" +
            "    \"name\": \"Vecrīga\",\n" +
            "    \"cod\": 200\n" +
            "}";
    private static final String RIGA_LATITUDE = "56.9496";
    private static final String RIGA_LONGITUDE = "24.0978";

    @Mock
    private RestTemplate restTemplate;
    @Mock
    private IpGeolocationResolvingService ipGeolocationResolvingService;

    @Mock
    private IpGeolocationDTO ipGeolocationRiga;

    @Test
    public void getCurrentWeatherData() throws IOException, GeoIp2Exception {
        WeatherDataProvidingServiceImpl weatherDataProvidingServiceImpl = new WeatherDataProvidingServiceImpl(restTemplate, ipGeolocationResolvingService, "12345");

        doReturn(ipGeolocationRiga).when(ipGeolocationResolvingService).getLocalhostGeolocation();
        doReturn(RIGA_LATITUDE).when(ipGeolocationRiga).getLatitude();
        doReturn(RIGA_LONGITUDE).when(ipGeolocationRiga).getLongitude();

        String weatherApiUrl = String.format("https://api.openweathermap.org/data/2.5/weather?lat=%s&lon=%s&appid=%s", RIGA_LATITUDE, RIGA_LONGITUDE, "12345");
        when(restTemplate.getForObject(weatherApiUrl, String.class)).thenReturn(WEATHER_DATA);
        String currentWeatherData = weatherDataProvidingServiceImpl.getCurrentWeatherData();

        assertEquals(currentWeatherData, WEATHER_DATA);
    }
}