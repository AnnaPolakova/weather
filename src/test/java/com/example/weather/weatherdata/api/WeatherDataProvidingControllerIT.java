package com.example.weather.weatherdata.api;

import com.example.weather.weatherdata.service.WeatherDataProvidingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class WeatherDataProvidingControllerIT {

    private static final String WEATHER_DATA = "{\n" +
            "    \"coord\": {\n" +
            "        \"lon\": 24.0978,\n" +
            "        \"lat\": 56.9496\n" +
            "    },\n" +
            "    \"weather\": [\n" +
            "        {\n" +
            "            \"id\": 211,\n" +
            "            \"main\": \"Thunderstorm\",\n" +
            "            \"description\": \"thunderstorm\",\n" +
            "            \"icon\": \"11d\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 500,\n" +
            "            \"main\": \"Rain\",\n" +
            "            \"description\": \"light rain\",\n" +
            "            \"icon\": \"10d\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"base\": \"stations\",\n" +
            "    \"main\": {\n" +
            "        \"temp\": 295.2,\n" +
            "        \"feels_like\": 295.63,\n" +
            "        \"temp_min\": 295.2,\n" +
            "        \"temp_max\": 298.66,\n" +
            "        \"pressure\": 1013,\n" +
            "        \"humidity\": 83\n" +
            "    },\n" +
            "    \"visibility\": 10000,\n" +
            "    \"wind\": {\n" +
            "        \"speed\": 2.57,\n" +
            "        \"deg\": 150\n" +
            "    },\n" +
            "    \"rain\": {\n" +
            "        \"1h\": 0.15\n" +
            "    },\n" +
            "    \"clouds\": {\n" +
            "        \"all\": 75\n" +
            "    },\n" +
            "    \"dt\": 1626277402,\n" +
            "    \"sys\": {\n" +
            "        \"type\": 1,\n" +
            "        \"id\": 1876,\n" +
            "        \"country\": \"LV\",\n" +
            "        \"sunrise\": 1626227425,\n" +
            "        \"sunset\": 1626289686\n" +
            "    },\n" +
            "    \"timezone\": 10800,\n" +
            "    \"id\": 6615326,\n" +
            "    \"name\": \"Vecrīga\",\n" +
            "    \"cod\": 200\n" +
            "}";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WeatherDataProvidingService weatherDataProvidingService;

    @Test
    public void getCurrentWeatherData() throws Exception {
        doReturn(WEATHER_DATA).when(weatherDataProvidingService).getCurrentWeatherData();

        this.mockMvc.perform(
                get("/weather"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.coord.lon").value("24.0978"))
                .andExpect(jsonPath("$.coord.lat").value("56.9496"))
                .andExpect(jsonPath("$.weather.[0].id").value("211"))
                .andExpect(jsonPath("$.weather.[0].main").value("Thunderstorm"))
                .andExpect(jsonPath("$.weather.[0].description").value("thunderstorm"))
                .andExpect(jsonPath("$.weather.[0].icon").value("11d"))
                .andExpect(jsonPath("$.weather.[1].id").value("500"))
                .andExpect(jsonPath("$.weather.[1].main").value("Rain"))
                .andExpect(jsonPath("$.weather.[1].description").value("light rain"))
                .andExpect(jsonPath("$.weather.[1].icon").value("10d"));
    }
}