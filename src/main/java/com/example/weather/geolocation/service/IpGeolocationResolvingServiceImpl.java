package com.example.weather.geolocation.service;

import com.example.weather.geolocation.dto.IpGeolocationDTO;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;

@Service
public class IpGeolocationResolvingServiceImpl implements IpGeolocationResolvingService {

    private DatabaseReader cityDbReader;
    private LocalIpAddressProvider localIpAddressProvider;
    private IpGeolocationService ipGeolocationService;

    @Autowired
    public IpGeolocationResolvingServiceImpl(LocalIpAddressProvider localIpAddressProvider,
                                             IpGeolocationService ipGeolocationService) throws IOException {
        InputStream cityDatabaseIn = getClass().getClassLoader().getResourceAsStream("GeoLite2-City.mmdb");
        if (cityDatabaseIn == null) {
            throw new IllegalArgumentException("IP to geolocation database was not found.");
        } else {
            cityDbReader = new DatabaseReader.Builder(cityDatabaseIn).build();
        }
        this.localIpAddressProvider = localIpAddressProvider;
        this.ipGeolocationService = ipGeolocationService;
    }

    @Cacheable(value = "ipGeolocationCache")
    @Override
    public IpGeolocationDTO getLocalhostGeolocation() throws IOException, GeoIp2Exception {
        String localIpAddress = localIpAddressProvider.getLocalIpAddress();
        return getIpGeolocation(localIpAddress);
    }

    private IpGeolocationDTO getIpGeolocation(String ipAddress) throws IOException, GeoIp2Exception {
        IpGeolocationDTO existingIpGeolocation = ipGeolocationService.getIpGeolocationByIpAddress(ipAddress);
        if (existingIpGeolocation != null) {
            return existingIpGeolocation;
        }
        InetAddress inetIpAddress = InetAddress.getByName(ipAddress);
        CityResponse cityResponse = cityDbReader.city(inetIpAddress);
        String latitude = cityResponse.getLocation().getLatitude().toString();
        String longitude = cityResponse.getLocation().getLongitude().toString();
        return ipGeolocationService.saveIpGeolocation(ipAddress, latitude, longitude);
    }
}
