package com.example.weather.geolocation.service;

import com.example.weather.geolocation.dto.IpGeolocationDTO;

public interface IpGeolocationService {

    IpGeolocationDTO getIpGeolocationByIpAddress(String ipAddress);

    IpGeolocationDTO saveIpGeolocation(String ipAddress, String latitude, String longitude);
}
