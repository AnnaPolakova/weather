package com.example.weather.geolocation.service;

import com.example.weather.geolocation.dto.IpGeolocationDTO;
import com.maxmind.geoip2.exception.GeoIp2Exception;

import java.io.IOException;

public interface IpGeolocationResolvingService {

    IpGeolocationDTO getLocalhostGeolocation() throws IOException, GeoIp2Exception;
}
