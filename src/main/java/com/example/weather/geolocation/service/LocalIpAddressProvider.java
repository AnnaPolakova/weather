package com.example.weather.geolocation.service;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

@Component
class LocalIpAddressProvider {

    public String getLocalIpAddress() throws IOException {
        URL checkIpUrl = new URL("http://checkip.amazonaws.com");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(checkIpUrl.openStream()));
        return bufferedReader.readLine();
    }
}
