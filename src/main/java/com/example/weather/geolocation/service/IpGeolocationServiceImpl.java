package com.example.weather.geolocation.service;

import com.example.weather.geolocation.dto.IpGeolocationDTO;
import com.example.weather.geolocation.model.IpGeolocationEntity;
import com.example.weather.geolocation.repository.IpGeolocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IpGeolocationServiceImpl implements IpGeolocationService {

    @Autowired
    private IpGeolocationRepository ipGeolocationRepository;

    @Override
    public IpGeolocationDTO getIpGeolocationByIpAddress(String ipAddress) {
        IpGeolocationEntity entity = ipGeolocationRepository.findByIpAddress(ipAddress);
        if (entity != null) {
            return new IpGeolocationDTO(entity);
        }
        return null;
    }

    @Override
    public IpGeolocationDTO saveIpGeolocation(String ipAddress, String latitude, String longitude) {
        IpGeolocationEntity entity = new IpGeolocationEntity(null, ipAddress, latitude, longitude);
        IpGeolocationEntity savedEntity = ipGeolocationRepository.save(entity);
        return new IpGeolocationDTO(savedEntity);
    }
}
