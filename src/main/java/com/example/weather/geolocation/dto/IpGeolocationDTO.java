package com.example.weather.geolocation.dto;

import com.example.weather.geolocation.model.IpGeolocationEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
public class IpGeolocationDTO implements Serializable {

    private static final long serialVersionUID = 4386737173478526788L;

    private final String ipAddress;
    private final String latitude;
    private final String longitude;

    public IpGeolocationDTO(IpGeolocationEntity entity) {
        this.ipAddress = entity.getIpAddress();
        this.latitude = entity.getLatitude();
        this.longitude = entity.getLongitude();
    }
}
