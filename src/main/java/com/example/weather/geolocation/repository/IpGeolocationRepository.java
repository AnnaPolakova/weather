package com.example.weather.geolocation.repository;

import com.example.weather.geolocation.model.IpGeolocationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IpGeolocationRepository extends JpaRepository<IpGeolocationEntity, Long> {

    IpGeolocationEntity findByIpAddress(String ipAddress);
}
