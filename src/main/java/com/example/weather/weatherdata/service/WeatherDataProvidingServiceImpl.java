package com.example.weather.weatherdata.service;

import com.example.weather.geolocation.dto.IpGeolocationDTO;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.example.weather.geolocation.service.IpGeolocationResolvingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Service
public class WeatherDataProvidingServiceImpl implements WeatherDataProvidingService {

    private RestTemplate restTemplate;
    private IpGeolocationResolvingService ipGeolocationResolvingService;
    private String weatherApiKey;

    @Autowired
    public WeatherDataProvidingServiceImpl(RestTemplate restTemplate,
                                           IpGeolocationResolvingService ipGeolocationResolvingService,
                                           @Value("${open-weather-map.api-key}") String weatherApiKey) {
        this.restTemplate = restTemplate;
        this.ipGeolocationResolvingService = ipGeolocationResolvingService;
        this.weatherApiKey = weatherApiKey;
    }

    @Cacheable(value = "weatherDataCache")
    @Override
    public String getCurrentWeatherData() throws IOException, GeoIp2Exception {
        IpGeolocationDTO localIpGeolocation = ipGeolocationResolvingService.getLocalhostGeolocation();
        String weatherApiUrl = String.format("https://api.openweathermap.org/data/2.5/weather?lat=%s&lon=%s&appid=%s", localIpGeolocation.getLatitude(), localIpGeolocation.getLongitude(), weatherApiKey);
        return restTemplate.getForObject(weatherApiUrl, String.class);
    }
}
