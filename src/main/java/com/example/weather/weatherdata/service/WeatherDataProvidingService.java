package com.example.weather.weatherdata.service;

import com.maxmind.geoip2.exception.GeoIp2Exception;

import java.io.IOException;

public interface WeatherDataProvidingService {

    String getCurrentWeatherData() throws IOException, GeoIp2Exception;
}
