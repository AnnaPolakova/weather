package com.example.weather.weatherdata.api;

import com.example.weather.weatherdata.service.WeatherDataProvidingService;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(value = "/weather", produces = "application/json")
public class WeatherDataProvidingController {

    @Autowired
    private WeatherDataProvidingService weatherDataProvidingService;

    @GetMapping
    public String getCurrentWeatherData() {
        try {
            return weatherDataProvidingService.getCurrentWeatherData();
        } catch (GeoIp2Exception ex) {
            throw new ResponseStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR, "Resolving geolocation by IP resulted in error.", ex);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR, "Providing current weather data resulted in error.", ex);
        }
    }
}
