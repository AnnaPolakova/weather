# README #

Execute JAR file with ```java -jar weather-0.0.1-SNAPSHOT.jar```

To get current weather data use ```localhost:8080/weather```

### Functional requirements

* http://checkip.amazonaws.com is used to get external local IP address;
* GeoLite2 database and MaxMind GeoIP2 Java API are used to map IP address to geolocation;
* OpenWeather API is used to get current weather data by geolocation.

### Non functional requirements

1. All services and the controller are covered by tests;
2. Exceptions are handeled in the controller, using ```ResponseStatusException```;
3. Ehcache is used for in-memory caching;
4. Derby embedded database is used for database caching, as well as for testing.